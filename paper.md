% OSMDeepOD - Object Detection on Orthophotos with and for VGI
% Samuel Kurath; Raphael Das Gupta; Stefan Keller
  Geometa Lab at Institute for Software, HSR; geometalab@hsr.ch
% January 2017
---
lang: en-GB
bibliography: references.bib
numberSections: true
sectionsDepth: -1
subfigGrid: true
...


# Abstract {.unnumbered}
<!--- How much is a lot? Can this be verified somehow? I can't find a reference to this later on either -->
A lot of the interesting information portrayed by aerial imagery is yet unused,
even though it could help enriching maps and improving navigation.
To make this information available,
objects such as buildings or roads
need to be recognized on images.
This is laborious to do entirely manually but non-trivial to perform computationally.
In this paper,
we present an automated method
for detecting
objects of a chosen class
(pedestrian crosswalks in our case)
on orthophotos.
The method can be adapted
for various classes of objects.
We solved this problem using a supervised machine learning approach with a convolutional neural network.
On a pre-trained neural network we re-trained the final layer for new categories with specific imagery and with crowdsourced geographic information from the OpenStreetMap project.
The result is an easily enhanceable and scalable application which is able
to search for objects on aerial imagery. We achieved an accuracy well over 95% on crosswalks and promising preliminary results for roundabouts.

Keywords: visual recognition, deep convolutional networks, aerial imagery, VGI, parallelism

# Introduction {#sec:intro}

Like in many other fields,
machine learning
has become a hot topic
in geography and geoinformation
including VGI.
It can be used
to derive conclusions
from geoinformation
or
to derive geoinformation
from geodata,
e.g. feature positions
from raster data
using image recognition techniques.
In this work
we focus on the latter.

As a proof of concept,
we chose the task
of completing
registered Swiss pedestrian crosswalks
in OpenStreetMap
using satellite and aerial imagery
(orthophotos).
For that,
we had to device
a method to detect crosswalks
as well as
a way
to feed results
back into OpenStreetMap
while adhering to
the OpenStreetMap community's
quality standard of
edits made
or reviewed
by humans.

The objective
of this research
was to
come up with a process
which accomplishes that
and which can easily be adapted
for other, similar feature classes.
Further,
for keeping OpenStreetMap up-to-date,
it should be possible to
repeat that process
on new orthophotos
when they become available.

This article is organized in 6 sections.
[Section @sec:intro] is introductory.
In [@sec:method]
we describe our method
and
in [@sec:results]
we present the results of
applying that approach to
pedestrian crosswalks
in Switzerland.
We critically discuss
both, method and results
in [@sec:discussion].
[@Sec:concl] summarizes
what this work achieves
and outlines
possible future research.
References are listed
in [@sec:refs].

## Motivation: Increase crosswalk coverage in OpenStreetMap
OpenStreetMap<sup>&reg;</sup>^[http://openstreetmap.org] is a worldwide community of mapping enthusiasts
and according to @fi4010001
"a well-known project in the field of Volunteered Geographic Information (VGI)"
(a term coined by @Goodchild2007)
also known as "crowd sourced geodata".
To the public, it makes available not just a web map, but also the underlying database of geographic information,
that anyone can edit.

Although the OpenStreetMap project initially set out to create a free street map (hence its name),
it today collects all geographical information (vector geometry with attributes/tags) deemed interesting
by the community members as long as that information is verifiable "on the ground".
Thus, many features not displayed on a typical street map can be found in the OpenStreetMap dataset,
including the positions of pedestrian crosswalks on a road axis.

While pedestrian crosswalk locations are vital for assisting pedestrian navigation
and might also become important for automated driving,
a crosswalk existing in reality but missing from the dataset
will not stand out as much to most mappers as a missing road would.
This makes it difficult to improve the completeness of the crosswalk location information in the dataset
even for mappers interested in this particular topic.

Satellite and aerial imagery are used for
assisting data collection when surveying on the field
and for directly tracing recognizable objects remotely.
However, manually searching the orthophotos
of even just a small country like Switzerland
for all instances of one feature such as pedestrian crosswalks
would be a very time-consuming and tedious task.
We decided to help increase the dataset completeness
by providing more automation
and thereby facilitating targeted editing.

## Challenge: Automated visual object detection
Although humans are very good at visual pattern recognition,  <!-- is this general knowledge? Maybe citation needed? -->
accurately detecting and identifying objects on a picture
can be difficult or&mdash;depending on
light conditions, shadows, and other objects that obscure the interesting aspects of the images&mdash;impossible,
even for them.  <!-- it has example images, is there even scientific information on this topic? May citation is needed -->
Consider for example the orthophotos in @fig:xw_examples showing pedestrian crossings
and compare them to the orthophotos in @fig:nonxw_examples that do not show pedestrian crossings.

![Crosswalk examples](./img/crosswalk_example.png){#fig:xw_examples width=200 height=100px}

![Non-crosswalk examples](./img/non_crosswalk_example.png){#fig:nonxw_examples width=200 height=100px}

Devising a computer program to do the image recognition is even trickier,
as traditional rule-based programming usually falls flat in this area.  <!-- Citation needed. Others must have failed while trying ;-) -->
^[For the specific case of pedestrian crosswalks,
one might expect
a spectral-analysis-based approach would work.
The frequency-based methods we evaluated in
[@keller2016erkennung]
though failed to beat the deep learning-based one,
with only fast Fourier transform coming close.]
But recent developments
in artificial intelligence (AI) research
have made computational object classification feasible.
Artificial neural networks,
a computation approach that loosely mimics the way
a biological brain works on the neuron and axon level,
have been a subject in AI research for a long time
[e.g. @rosenblatt1957perceptron; cf. @russel1996nnhistory for a brief history].
In our case,
the input is an image (a small part of an orthophoto)
and the desired output
is a classification of the object(s) seen there,
albeit a simple one:
A crosswalk or not a crosswalk.

In recent years, the best results in image recognition have been achieved by Deep Convolutional Networks [@NIPS2012_4824].
These are a kind of further developed neural networks, designed to mimic the visual cortex.
They get their name from stacking many convolutional layers.
A convolutional layer has neurons
that each have a convolution matrix
(called "kernel" or "filter")
as their learned parameter.
Each of these layers acts as a feature detector
working on a local-ish feature map.
For the first layer,
the input feature map is simply the pixel colour values,
early layers will recognize simple locally confined features
like lines, edges and patches of colour.
Later layers will recognize more and more visually complex features
as arrangements of simpler features from the layer before them,
until the output layer produces the classification of the whole image.

The chosen approach was not without challenges:
We had to
(1) get enough training data for the neural network,
(2) get the neural network's training time short enough
to allow for training the network with different parameters,
so that we were able to improve and fine-tune the result,
(3) deal with a huge amount of satellite imagery,
and
(4) make the results available in OpenStreetMap
without breaking the community's rules
against automated imports.

## Related work

Mnih & Hinton have applied machine learning
to orthophotos
for per-pixel labelling
for some time now.
They have improved road detection
with the help of a deep network
[-@Mnih2010]
and studied the effects
of lacking ("noisy") learning data
and devised methods
that allow the training process
to better cope with the noise
[-@ICML2012Mnih_318].
This is particularly relevant
when learning from VGI,
where "(asymmetric) omission noise"
(object visible on orthophoto,
but missing from the VGI)
and "registration noise"
(different locations of an object
on the orthophoto vs. the VGI)
are prevalent,
especially if the detection is attempted
to improve and complete that very VGI
for the chosen object class of interest.

@Chen:2017:DDL:3041021.3054250
have applied deep learning
with positive learning samples
determined by
already mapped buildings in OpenStreetMap
and negative samples
from data generated by
the MapSwipe mobile app
where users indicated
that an area did _not_ contain buildings.
They turned
this supervised learning setup
into an active learning one
by manually labelling the tiles
where their network disagreed
with MapSwipe-provided labels
and using these samples
to re-train the network
and thereby
significantly improved its performance
^[precision, recall, F1 score and accuracy; _not_ the computational performance],
though not quite
reaching that of MapSwipe volunteers.

In contrast to these related works,
we did not devise our own
neural network design.
Instead we chose
an existing network already proven successful
for (non-orthophoto) image classification
and focused on
how to get training data,
how to avoid having to train
a full Deep Neural Network,
how to automate
and how computationally optimize
the procedure.


# Method {#sec:method}
To detect objects on orthophotos we applied the approach visualized on @fig:methodology.
In the following subsections we will describe the individual steps in depth.

![Method overview:
    Positive samples
((1) orthophotos at locations
    of known crosswalks (from OSM)
    with
(2) false positives manually discarded)
    and
    semi-manually collected
    negative samples
    (red folder icon)
    are
(3) used to re-train a pre-trained dCNN.
    With that,
(4) orthophotos along OSM streets
    are
(5) classified.
    Crosswalks already in OSM
    are
(6) pruned from the results
    and nearby matches merged.
    Remaining results are then
(7) fed through MapRoulette
    for crowd-sourced review
    and
    for manual integration into OSM.
](./img/svg/overview.svg){#fig:methodology}

At step (1) in @fig:methodology we select images from a satellite imagery source with the help of OpenStreetMap
which serve as the dataset for the training mechanism of the neural network.
The dataset consists of a positive and a negative subset,
containing images
with or without objects we are looking for, respectively.
In step (2),
false positives
are manually removed
from
the automatically collected
positive subset.

This dataset is then used
to retrain a neural network (step (3)),
which can classify images
into the different dataset categories (5).
The images to classify
are obtained
by moving a sliding window
over the area of interest (4).

The midpoints of the windows
where objects of the class of interest
have been recognized by the neural network
are aggregated (to eliminate duplicates)
and treated as the object locations.
After (6) removing locations
corresponding to instances
already in the OpenStreetMap data set,
we use the coordinates
to generate tasks and challenges for MapRoulette.
MapRoulette users then review the detected objects
and edit the genuine ones into OpenStreetMap (7).

## Acquiring training data

A huge amount of data is needed to train
a large neural network like the Deep Convolutional Network.
Significantly less training data is needed
for re-training an already pre-trained deep neural network
(cf. next section),<!-- TODO: MarkDown-ify -->
but the number of examples per class should still be in the thousands
[@keras_retrain_tutorial].
To efficiently obtain a training set of sufficient size,
we combine existing OpenStreetMap data with aerial imagery:
Using the fact that _some_ pedestrian crosswalks are already mapped in OpenStreetMap
and that many of these are visible on aerial imagery,
we are able to cut out sections from the orthophotos
at the positions of crosswalks already present in the dataset.
(@Fig:training_data_selection and (1) in @fig:obtain_positive_samples)
These sections are likely to show a pedestrian crossing
and thus
are promising to provide a solid positive training set.
For obtaining the crosswalk positions in OpenStreetMap
we query Overpass [@osmwiki:overpass],
an easy-to-use application programming interface (API)
for accessing OpenStreetMap data.

![Positive samples are obtained by
(1) using orthophotos at known crosswalk locations and
(2) manually sorting out false positives and unusable images.](./img/svg/obtain_positive_samples.svg){#fig:obtain_positive_samples}

![Visualization of the training data selection:
The position of a crosswalk in OpenStreetMap is indicated by a blue dot.
The red square is the border for the 50&times;50 pixel size section
to be be cropped out of the aerial imagery
shown in the background of this visualization.](./img/collect_images.png){#fig:training_data_selection width=80 height=80px}


Unfortunately, a manual check of the collected images
((2) in @fig:obtain_positive_samples)
is still needed before they can be used as training data
since the result is not perfect.
The object in question might not be visible on the image at all,
due to either OpenStreetMap or the aerial imagery being out of date
or because of different translational offsets
placing the object outside the cropped-out section.
For our example of detecting crosswalks,
we also decided to discard images
where a car, a shadow or a tree covers the target,
as is the case in @fig:occlusion_example.

![Example image of a crosswalk halfway covered by a tree,
which should not be a part of the training set.](./img/collect_images_tree1.png){#fig:occlusion_example width=80 height=80px}

Having collected the positive samples with
the target object on them, to complete our training set
we also need negative samples.
Because the search algorithm is more likely  <!-- is it a search algorithm? -->
to encounter areas without the target object
and because these areas will vary more broadly
than the areas containing the target object,
we need even more
(and more varied)
negative samples
than positive ones.

To gather negative samples efficiently, we made an application
which takes small screenshots along the user's mouse movement.
Using an aerial map these samples can be collected
simply by moving the cursor over the map at the correct resolution (zoomlevel).
(Illustrated in @fig:pyntscreen.)
Of course, hovering over the target objects must be avoided.

![A visualization of the sample acquisition process with a cursor moving over satellite imagery.
Every red rectangle represents a cropped-out image.](./img/pyntscreen.png){#fig:pyntscreen width=220 height=89px}

The combination of these two methods to collect images
allows us to produce a dataset within reasonable time
and is applicable to a wide variety of objects.


## Re-training a pre-trained dCNN
The Deep Convolutional Networks do fit our detection task,
but very deep networks with many hidden layers require a lot of training data.
With parameter counts around 20 million
[@DBLP:journals/corr/Chollet16a],
these networks are prone to overfitting:
If the training set is not large enough,
the networks are unable to generalize enough
to handle unknown situations.
The excess time and effort needed
to acquire these amounts of training data
and the long processing time required
to train a network with them
can be avoided by resorting to
an already trained neural network
and only re-training the last layer
for the new classification task.
(@Fig:retrain_dCNN)
This process is called transfer learning [@DBLP:journals/corr/YosinskiCBL14, @Oquab_2014_CVPR]
and is possible because the first layers of Convolutional Networks do very generic tasks like edge and shape detection [@DBLP:journals/corr/ZeilerF13].
The "How to Retrain Inception's Final Layer for New Categories" guideline from TensorFlow [@tf_retrain_tutorial]
advices how to do that.
We found that
the re-train approach reduces the training time on a Tesla K40m GPU by up to four hours.  <!-- data citation? -->

![Re-training (3) of the Inception v3 network already pre-trained on ImageNet data](./img/svg/retrain_dCNN.svg){#fig:retrain_dCNN}

To handle the detection part, we use TensorFlow[^tf], an open source software library for numerical
computation, machine learning and simplification of GPU usage.
TensorFlow provides various pre-trained neural network models for image classification.
For our task we used a pre-trained Inception-v3 model with 23&#8239;853&#8239;833 parameters [@DBLP:journals/corr/Chollet16a].
The network is learned from the images of the ImageNet academic competition,
which has about a million pictures divided into 1&#8239;000 categories.
Inception-v3 achieves 3.58% top-5 error on the validation set on this competition.
That means that the network
had to predict the five classes
most likely to describe the image,
and that the image's real class
was amongst these five predicted ones
in 96.42% of all cases.
To put this into perspective,
Andrej Karpathy went through a subset of the validation set himself
[@blogKarpathy_2014_human]
and scored a human top-5 error rate of 5.1%
(1.52% worse than Inception-v3).
As a proof of concept, we re-trained the Inception-v3 model [@DBLP:journals/corr/SzegedyVISW15] on 8&#8239;242 images of crosswalks and 40&#8239;463 images of non-crosswalks,
which reached 98.5% accuracy on our validation set.

[^tf]: <https://www.tensorflow.org/>

## Object detection: Searching for crosswalks
For the search of crosswalks, the sequence of the detection is structured as follows:
<!-- This section has already been described above, in "Method" -->
The area to be analysed is passed to the system as a large bounding box.
This large bounding box is then split into smaller bounding boxes
which get published to the queue as jobs which have to be processed.

![A sliding window along OSM roads (4) determines the orthophoto excerpts to be used as inputs for the classification by the dCNN (5)](./img/svg/search_crosswalks.svg){#fig:search_crosswalks}

If a so-called worker starts processing a job, it collects all streets from OpenStreetMap
for that job's (small) bounding box. ((4) in @fig:search_crosswalks) If there aren't any streets in the current small bounding box the job is already finished,
else the corresponding orthophotos (@fig:job_bbox_aerial) are being downloaded.

<div id="fig:sliding_window_steps">
![Aerial imagery for one job's bounding box](./img/process_1.png){#fig:job_bbox_aerial width=50% height=143px}
&nbsp;
![Corresponding street data from OSM (overlaid as blue lines)](./img/process_2.png){#fig:job_bbox_streets width=50% height=143px}

![50&times;50 pixel cutouts (shown as red squares) along the streets](./img/process_3.png){#fig:job_bbox_cnn_inputs height=143px}
&nbsp;
![Some of the cutouts (centres marked by green dots) have been classified as containing crosswalks](./img/process_4.png){#fig:job_bbox_xwalks height=143px}

The steps for object detection with sliding windows along streets
</div>

In @fig:job_bbox_streets,
you see the street data
overlaid over the orthophoto.
Along those streets,
the worker
has to cut out 50&times;50 pixel sized images from the orthophotos,
which is the size the convolutional neural networks needs as input parameter. (@Fig:job_bbox_cnn_inputs)
Finally, the detection algorithm decides for each image whether it contains a crosswalk or not. (@Fig:job_bbox_xwalks, (5) in @fig:search_crosswalks)

When the classification is done, all detected crosswalks within a radius of 5 meters get merged
(part of (6) in @fig:vet_found_crosswalks).
This has to be done because it is possible that some crosswalks are larger than one cut out image and appear on neighbouring or overlapping images.
Also, our mode for searching along streets
isn't yet sophisticated enough
to not search areas twice
where streets cross.

![Detected crosswalks close to each other get merged and those close to existing ones in OSM eliminated (6). The remaining ones are vetted by MapRoulette volunteers and manually edited into OSM if deemed genuine (7).](./img/svg/vet_found_crosswalks.svg){#fig:vet_found_crosswalks}

## Optimizations
When faced with the task of finding all crosswalks in Switzerland, we are confronted with an area of 41&#8239;285 km²,
which would have corresponded to roughly 400 million images with a size of 50&times;50 pixel at a resolution of 0.3 meter per pixel.
To speed up the process,
we parallelize the image recognition
and
we also reduce
the amount of data
that needed
to be classified.

TensorFlow already makes use of
vector processing and multiple cores
within the classification
of a single image
and for training.
However,
to use our hardware
at full capacity
we further do
detection of several images
in parallel
by using a message queue[^rq]
from which multiple
"worker" processes
pull their tasks.

[^rq]: specifically, RQ (<http://python-rq.org/>)

Because our detection algorithm is confronted with a large number of images to classify
we looked for opportunities to reduce that number.
One technique we have implemented as an option
is to only analyse images along streets.
This is sufficient
when looking for crosswalks or roundabouts,
as these can only occur along streets.
Thus, this approach decreases the amount of images significantly without deterioration of the detection rate.
When this option is enabled,
our algorithm checks for known streets
before downloading the orthophoto tiles,
so that only tiles along streets are downloaded.
To get the street positions,
we again use OpenStreetMap
with help of the Overpass API [@osmwiki:overpass].

The step size
for moving the sliding window
along the streets
or over the complete
ortophoto tile
also heavily influences
the number of images
to classify.
We chose a translation
of 0.66 of the windows' side length
and found that
this gives enough overlap
to not miss too many features.

A way to reduce the data further
is to work on the lowest resolution level of the orthophotos
still sharp enough to represent
the important features of the objects to be detected,
resulting in fewer and smaller images to be analysed.
For crosswalks,
this turned out to be zoomlevel&nbsp;19.
It seems that for roundabouts,
even just zoomlevel&nbsp;18
might suffice.

## Crowd-sourced verification and integration
The goal of our work is not only to detect objects on orthophotos, but also to make the new gained information available to the general public and specifically to the OpenStreetMap community.
An obvious way to achieve this
is to integrate the newly found crosswalks
into the OpenStreetMap dataset.
However, OpenStreetMap only accepts data which are verified by humans,
so just dumping the data into an automated import
was out of question.
Even though the detection rate is very high,
there still are false positives
in our detected crosswalk data
and we don't want them
to pollute the OpenStreetMap data.
Further, in OpenStreetMap,
a crosswalk is tagged
on the individual node (i.e. point)
of the way (linestring)[^osm_data_model]
representing the crossed street section
[@osmwiki:tag_highway_crossing],
an information not available
in our result dataset. <!-- this might do with a deeper explanation or at least a reference how OSM data is being stored. -->

There are gamification tools
that integrate data into OpenStreetMap
or help users with editing data.
One of them, MapRoulette,
is an open source project
and web platform on <http://maproulette.org/>
which allows everyone
to create challenges with multiple tasks.
These challenges can then be solved by other users (or, "the crowd")
by editing OpenStreetMap using their own accounts.
<!-- Is this entire section needed for this paper? seems rather a description of another product than relevant IMHO -->
In contrast
to some other OpenStreetMap gamification tools
the editing functionality is not built into
the MapRoulette platform itself.
Instead, the user can choose
which generic OpenStreetMap editor to launch
amongst several popular ones
that MapRoulette can interact with.

MapRoulette is meant to be used from home,
rather than in the field
so tasks should be solvable
with only the information
provided by available imagery.
Deciding from the imagery
whether there is a crosswalk in a small area
(step (7) in @fig:vet_found_crosswalks)
is a suitable task for MapRoulette,
because crosswalks usually only appear along paved streets
(which are themselves usually easy to discern on aerial imagery),
and have a distinctive pattern
and a strong colour contrast with the environment.
If a task doesn't require map editing to be completed
(e.g. because our software mis-detected a crosswalk where there is none)
or when they can't solve a task
(e.g. because they cannot discern whether there is a crosswalk on the picture, or where)
users can indicate so on the MapRoulette platform.

[^osm_data_model]: For the OpenStreetMap data model cf. [@osm_book, p. 51ff] or [@osmwiki:elements]

# Results {#sec:results}
This project yielded
(a)
a workflow for detecting objects on orthophotos
and
(b)
a software application implementing this workflow,
as well as
(c)
positions of 18&#8239;036 potential crosswalks
that weren't already mapped in OpenStreetMap,
found by applying our approach
region by region
over the complete area of Switzerland.

We've put up 20 challenges on MapRoulette
to get these positions vetted
and
(if genuine)
edited into OpenStreetMap.
We observed
the weekly increase of crosswalks
in OpenStreetMap within the area of Switzerland,
before and during these challenges,
and their positive influence
was clearly visible:
During 33 weeks before the first challenge started,
we had an average growth of 63.2 crosswalks per week.
During the first 24 week of the challenges,
we measured a weekly increase of 370.5 crosswalks.
Within those 24 weeks,
the number of crosswalk in Switzerland
raised from 41&#8239;788 to 50&#8239;584.[^safari]
This means that
almost one out of five crosswalks
present in OpenStreetMap
after 24 weeks of our challenges
has been added during that period.

[^safari]: See <http://zebrastreifen-safari.osm.ch/>
           for an up-to-date graph of recent development.

We are therefore confident
that we've reached our goal
of helping to increase
the OpenStreetMap dataset completeness
with regard to
pedestrian crossings.


# Discussion {#sec:discussion}

It must be noted that the very high accuracy of 98.5%
reported by the training
applies to a validation set
sampled from the input data
and stashed apart
by TensorFlow's `retrain.py` script.
While this validation data
has not been used by the script
for the actual training,
so that the high value
should not be the result
of overfitting to the samples seen,
the input data to the script
could have been biased as a whole,
so this result may not correspond
to the real accuracy
of the actual classification
performed after the training
in the object detection steps.

To estimate the actual accuracy,
the error rate
(false positives and false negatives
in relation to the total amount
of classified images)
could be approximated
by using
the number of crosswalks
previously in OpenStreetMap
that haven't been re-found by the detection
as a lower bound for the false negatives.
A lower bound
for the number of false positives
is more directly available
if the assessment
by MapRoulette volunteers
is treated as the ground truth,
which we guess
should be close enough
to reality.

The decision to leave
partially covered crosswalk pictures
out of the training set
was made arbitrarily
without being based
on actual findings.
It should be tested
whether including
these pictures
in the training
improves or degrades
the detection rate.

# Conclusion and Outlook {#sec:concl}
We have demonstrated that
software developers and (GIS) domain experts
can apply state-of-the-art computer vision technology
to automate
previously manual tasks
of their field.
The libraries and frameworks available nowadays
have enabled us to do so
without extensive prior AI knowledge.

While (near-)ready-made routines
from the used framework
were employed for the classification part
at the core of our process,
our contribution comprises of
the workflow and steps around that:
(a)
Ways of efficiently and semi-automatically
obtaining sufficient training data,
(b)
framing the object search/detection/coarse-localization problem
as a classification of focal data within candidate regions,
(c)
using domain knowledge
for
limiting the candidate region
within the complete area of interest
to decrease the classification expenditure,
(d)
verification and integration
of the results
into OpenStreetMap
in accordance with the community's rules
through use of
an existing targeted crowd-editing platform,
and
(e)
some (limited) automation
of the individual steps
as a re-usable
and adaptable
open source application
available at
<https://github.com/geometalab/OSMDeepOD>.

With the flexibility and the various configuration parameters of OSMDeepOD, it is possible to search for other objects like streets,
buildings, swimming pool, photovoltaic, football fields, tennis courts, agricultural area, and lots more.
Collecting a fitting data set and re-training the Convolutional Network should be all is needed.

Other varieties of object recognition on satellite imaginary could be to use oblique photographs and detect the floors of buildings.
Which is a very helpful information for 3D visualization of maps.
Or it could be very interesting to localize objects on the images and get their shapes.

For further work to improve the detection, a good point to start would be to have a look at the newest designed neural network models [@DBLP:journals/corr/SzegedyIV16].
Advancements in machine learning have been so fast that better networks are created almost every week.
For localization tasks, Fast Region-based Convolutional Network [Fast R-CNN, see @DBLP:journals/corr/Girshick15] would seem like the right place to start.
The goal of Fast R-CNN is to classify multiple objects on images and to be able to draw a bounding box around them [@UijlingsIJCV2013].

# References {#sec:refs}

<div id="refs"></div>

## Images
All orthophoto imagery shown in our figures or their backgrounds is taken from

- Federal Office of Topography (SwissTopo): SWISSIMAGE - The Digital Color Orthophotomosaic of Switzerland, https://shop.swisstopo.admin.ch/en/products/images/ortho_images/SWISSIMAGE

All visualizations on top of these are our own work (lineart etc.) or screenshots taken by us (mouse pointer).
