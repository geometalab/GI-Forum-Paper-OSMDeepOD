.PHONY: all
all: AGIT2017_FP_14239_2.pdf AGIT2017_FP_14239_2.docx

CSL := https://raw.githubusercontent.com/citation-style-language/styles/master/apa.csl
COMMON_PANDOC_OPTIONS := --number-sections --smart -f markdown --latex-engine=xelatex --filter pandoc-crossref --filter pandoc-citeproc --filter ./pandoc-svg.py --csl=$(CSL)

AGIT2017_FP_14239_2.pdf: paper.md references.bib
	pandoc -M numberSections=false $(COMMON_PANDOC_OPTIONS) -o $@ $<

AGIT2017_FP_14239_2.docx: paper.md references.bib
	pandoc $(COMMON_PANDOC_OPTIONS) -o $@ $<
