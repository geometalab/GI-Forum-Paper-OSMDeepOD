# GI Forum Paper OSMDeepOD
Paper for the GI Forum about OMSDeepOD

## Project Board
To handle the Issues we us the project board from Gitlab.  
[Link to the board](https://gitlab.com/geometalab/GI-Forum-Paper-OSMDeepOD/boards)

## Notes
### Content
 - overview
 - process for success
 - result (confusion matrix)
 - visions, possibilities
 - problems (imagery timestamp)


## Links
 - http://www.gi-forum.org/
 - http://www.gi-forum.org/images/Forms/Documents2017/2017Journal_Instructions_for_Authors_FINAL.pdf
 - https://github.com/geometalab/OSMDeepOD/issues/116
